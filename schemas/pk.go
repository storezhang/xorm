// Copyright 2019 The Xorm Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package schemas

import (
	"bytes"
	"encoding/gob"
	`fmt`
	`strings`

	"xorm.io/xorm/internal/utils"
)

type PK []interface{}

func NewPK(pks ...interface{}) *PK {
	p := PK(pks)
	return &p
}

func (p *PK) IsZero() bool {
	for _, k := range *p {
		if utils.IsZero(k) {
			return true
		}
	}
	return false
}

func (p *PK) ToString() (str string, err error) {
	var sb strings.Builder

	for _, pp := range *p {
		switch pp.(type) {
		case int8, uint8, int, uint, int32, uint32, int64, uint64, float32, float64:
			sb.WriteString(fmt.Sprintf("%v", pp))
		default:
			buffer := new(bytes.Buffer)
			encoder := gob.NewEncoder(buffer)
			if err = encoder.Encode(pp); nil != err {
				return
			}
			sb.WriteString(buffer.String())
		}
		sb.WriteString("-")
	}
	str = strings.TrimRight(sb.String(), "-")

	return
}

func (p *PK) FromString(content string) error {
	dec := gob.NewDecoder(bytes.NewBufferString(content))
	err := dec.Decode(p)
	return err
}
